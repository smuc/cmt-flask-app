from flask import Flask, render_template, request, session, redirect, send_file 
from cmt.training import Training, async_training
from cmt.interfacing import CMT_Interface # import CMT_Interface class from interfacing.py file
from cmt.labeling import save_labeled_data, sort_data_4_labeling
from smuc_kitchen.training import get_list_of_models
from flask_executor import Executor

# initialise CMT_Interface class with name of save data file
interface = CMT_Interface() 

Pass = "WeLoveAutonomy"
admin_password = "admin_pass"

# initialise flask app
app = Flask(__name__)

app.secret_key = 'kwer!wh83£$47dh82u&wh28?'

print("#############################")
print("#############################")
print("Password: ", Pass)
print("#############################")
print("#############################")
 
executor  = Executor(app)
# Showing use of asyncio.create_task()
# Total runtime is 2 seconds




# Renders the basic template as the index
@app.route("/", methods=['GET', 'POST'])
def index():

    #return render_template("labeling.html")  

    # if logged in render  nav page
    if session.get('logged_ in') and session["logged_ in"]==True:
        return render_template("Nav.html")

    # on l0gin button
    if request.method == "POST":
        # get password login
        pw = request.form.get("pw") 
        # if password matches
        if pw == Pass:
            # set session as logged in
            session["logged_ in"]=True
            # render nav
            return render_template("Nav.html")            
        
        
    return render_template("index.html")


# Renders the pickup form from dynamic values
# Curently not in use
@app.route("/pickup" ,methods=['GET', 'POST']) 
def pickupform():

    # if not logged in redirect to main page
    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")

    global interface

    if request.method == 'POST':
        # add form data to the data class and return the display values
        display_values =  interface.add_pickup_data(request.form.items())
        # Render response page template with form data if request method is post 
        return render_template("responsepage.html", values=display_values)
    
    return render_template("pickupform.html", dynamic_labels=interface.json["food_labels"][2:])


# Renders the kitchen form from dynamic values
@app.route("/kitchen",methods=['GET', 'POST'])
def kitchenform():

    # if not logged in redirect to main page
    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")
    
    global interface

    if request.method == 'POST':
        # Add form data to interface class and get display data back.
        display_values =  interface.add_kitchen_data(request.form.items())
        # Render response page template with form data if request method is post 
        return render_template("responsepage.html", values=display_values)
    
    # Render form template with dynamic values
    return render_template("kitchenform.html", dynamic_labels=interface.json["food_labels"],dynamic_options=interface.json['kitchen_labels'])


@app.route("/overview")
def overview():


    # if not logged in redirect to main page
    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")

    global interface  # Indicates that 'interface' is a global variable

    #Get the kitchens array direclty from CMT_Interface 
    kitchens = interface.kitchens

    #Initialize an empty list to store display values for all kitchens
    kitchens_display_values = []

    # Iterate through each kitchen in the kitchens array
    for kitchen in kitchens:
    #Get the display value for the current kitchen
        display_val = kitchen.get_display_val()
    # Append the display value to the list
        kitchens_display_values.append(display_val)
    # Return the list of display values for all kitchens 
    return render_template("overview.html", kitchens_display_values=kitchens_display_values, kitchen_labels=interface.json['kitchen_labels'])

@app.route("/labeling" ,methods=['GET']) 
def labeling():


    # if not logged in redirect to main page
    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")
    
    global interface
    #executor.submit(
    sort_data_4_labeling(interface)

    
    return render_template("labeling.html")


@app.route("/training",methods=['GET', 'POST'])
def training():

    global interface

        # if not logged in redirect to main page
    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")
    elif interface.training_now == True:
        return render_template("trainingerror.html")
    elif not session.get('training') or session["training"]==False:

        # create training class
        training_session=Training()

        # gets the info of all the datasets
        dataset_info = training_session.get_dataset_info("./Data/participant_labeling/")
    
        
        if request.method == 'POST':
            interface.training_now = True


            training_session.setting_from_form(request.form, dataset_info)
            # Set it to train with the output folder
            # execute the task
            future = executor.submit(training_session.train, 'trained_models')

            future.add_done_callback(trained_callback)

            session["training"]=True

            return render_template("trainingerror.html")

        # for the drop down selection
        list_of_models = get_list_of_models()

    # use like:
        hyper_params = training_session.get_hyper_params()

    
        # Render form template with dynamic values
        return render_template("training.html", hyper_params=hyper_params, list_of_models=list_of_models, dataset_info=dataset_info)
    else:
        session["training"]=False
        return redirect("/model")

def trained_callback(future):
    global interface
    summary = future.result()

    interface.model_infos.add_model_from_summary(summary)
    interface.training_now = False


@app.route("/model")
def model():

        # if not logged in redirect to main page
    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")

    global interface


    # gets the model infor for viewing in a nice class
    model_infos = interface.model_infos


     # Render form template with dynamic values
     
    return render_template("model.html", model_infos=model_infos, current_model = interface.running_model_name)


# downloads the 
#@app.route('/download')
def download_file():

    if  not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")

    global interface

    interface.zip_data()

    #path = data.working_file

    #return redirect("/zip")
    return send_file('./tmp/Data.zip', as_attachment=True)


@app.route('/saveJson', methods=['POST']) 
def save_labeling_json(): 

        # if not logged in redirect to main page
    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")
    
    global interface

    json_data = request.get_json()

    save_labeled_data(json_data=json_data)
    
    return "Saved Thanks for the samples!"

@app.route('/delete-model', methods=['POST']) 
def delete_model(): 

    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")
    global interface

    json_data = request.get_json()

    interface.delete_model(json_data['model_name'])
    
    return "Deleted"

@app.route('/select-model', methods=['POST']) 
def select_model(): 

    if not session.get('logged_ in') or session["logged_ in"]!=True:
        return redirect("/")
    global interface

    json_data = request.get_json()

    interface.load_model_to_run(json_data['model_name'])
    

    return "selected"

    
if __name__ == "__main__":
    # run the smuc_app function and get the app

    # run it!
    app.run( threaded=True)

    
