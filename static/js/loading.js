function showLoading() {
    var form = document.getElementById('training-form');
  
    for(var i=0; i < form.elements.length; i++){
    if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
        return false;
        }
    }
    
    // Show loading overlay
    document.getElementById("loadingOverlay").style.display = "block";
  
}


