function select_model(object) { 

    let json = {}; // new  JSON Object
  
    json.model_name = object.value

    if (confirm("Are you sure you want to use " +json.model_name+" to distribute?") == true) {

    
      $.ajax({ 
          url: '/select-model', 
          type: 'POST', 
          contentType: 'application/json', 
          data: JSON.stringify(json), 
          success: function(response) { 
              console.log("using ", object.value)
              window.location.reload();
          }, 
          error: function(error) { 
              console.log(error); 
          } 
      }); 
    }
    

  
} 

function delete_model(object){

  let json = {}; // new  JSON Object
  
  json.model_name = object.value

  if (confirm("Are you sure you want to delete " +json.model_name+"?") == true) {

    $.ajax({ 
        url: '/delete-model', 
        type: 'POST', 
        contentType: 'application/json', 
        data: JSON.stringify(json), 
        success: function(response) { 
          window.location.reload();
      }, 
        error: function(error) { 
            console.log(error); 
        } 
    }); 

  } else {

    return

  }




} 