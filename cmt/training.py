import os
from smuc_kitchen.training import get_list_of_models, train_model
from smuc_kitchen.utils.get_device import get_device
from smuc_kitchen.utils.save_load import load_model_for_inference, save_summary

from multiprocessing import Process

class async_training:

    def __init__(self, training_session, form, datasets, interface):
        p = Process(target=self.train, args=[training_session, form, datasets, interface])
        p.daemon = True                       # Daemonize it
        p.start()    
        #self.cmt = cmt    
        #self.train()
                             # Start the execution

    def train(self,training_session, form, datasets, interface):
        
        # Set training settings in session class
        training_session.setting_from_form(form, datasets)
        # Set it to train with the output folder
        summary = training_session.train('trained_models')


        interface.model_infos.add_model_from_summary(summary)
        interface.training_now = False
        # redirect to model selection

class Training:

    def __init__(self):

        self.labeldir="Data/participant_labeling"
        self.lr = 0.00001
        self.epochs = 700
        self.layers = [255,255]
        self.model = get_list_of_models()[0]
        self.num_datasets = -1

    def get_labeled_data(self):
        print(self.labeldir)

    def get_hyper_params(self):

        hyper_params = ["LR (0.001-0.00001)", "Epochs (300-1000)", "Layers ([128,128])", "Model Type", "Name"]

        return hyper_params
    
    def get_dataset_info(self, this_path):

        datasets = Datasets_Info()

        file_names = os.listdir(this_path)

        self.num_datasets = len(file_names)

        for name in file_names:
            datasets.add_data( this_path, name)
        

        datasets.sort_by_date()
        return datasets
    
    def setting_from_form(self, form_data, dataset_info):

        self.data_paths = []
        print(dataset_info.paths)
        for key, value in form_data.items():
            print(key, value)
            if key == 'lr':
                break
            index = int(key)
            
            self.data_paths.append(dataset_info.paths[index])

        self.lr = float(form_data['lr'])
        self.num_epochs = int(form_data['epochs'])

        self.layers = [ int(l) for l in form_data['layers'].replace("]", "").replace("[", "").split(",") ]
        self.model = form_data['model']
        self.name = form_data['name']

    def train(self, save_path):
        get_device()

        model, summary = train_model(
        data_paths=self.data_paths,
        lr=self.lr,
        epochs=self.num_epochs,
        layers=self.layers
        )

        summary.name = self.name

        save_path = os.path.join(save_path,self.name)

        save_summary(save_path, summary)

        return summary





class Models_Info():

    def __init__(self):
        self.names = []
        self.paths = []
        self.img_paths = []
        self.epochs = []
        self.lr = []
        self.num_train_examples = []
        self.datetimes = []
        self.summarys_string=[]
        self.final_accuracies=[]


    def add_model_from_file(self, path, name):

        path = os.path.join(path,name)
        _, model_summary = load_model_for_inference(path)

        self.paths.insert(0,path)
        self.epochs.insert(0,model_summary.epochs)
        self.lr.insert(0,"{:f}".format(model_summary.lr))
        self.num_train_examples.insert(0,model_summary.num_training_examples)
        self.datetimes.insert(0,model_summary.datetime)

        img_path = os.path.join('static/images/graphs/', name+'.png')
        self.img_paths.insert(0,img_path)
        model_summary.summary_plot.savefig(img_path, bbox_inches='tight')

        if model_summary.name != "":
            self.names.insert(0,model_summary.name)
        else:
            self.names.insert(0,name)

        self.final_accuracies.insert(0,model_summary.final_accuracy.item())

    def add_model_from_summary(self, model_summary):

        path = os.path.join('./trained_models/', model_summary.name)
        self.paths.insert(0,path)
        self.epochs.insert(0,model_summary.epochs)
        self.lr.insert(0,"{:f}".format(model_summary.lr))
        self.num_train_examples.insert(0,model_summary.num_training_examples)
        self.datetimes.insert(0,model_summary.datetime)

        img_path = os.path.join('static/images/graphs/', model_summary.name+'.png')
        self.img_paths.insert(0,img_path)
        model_summary.summary_plot.savefig(img_path, bbox_inches='tight')

        self.names.insert(0,model_summary.name)
 
        self.final_accuracies.insert(0,model_summary.final_accuracy.item())

    def delete_model(self, this_name):

        indexs_2_del = []

        for i, name in enumerate(self.names):
            if name == this_name:
                indexs_2_del.append(i)

        for indx in indexs_2_del:

             
            del self.paths[indx]
            del self.epochs[indx]
            del self.lr[indx]
            del self.num_train_examples[indx]
            del self.datetimes[indx]

            del self.img_paths[indx]
            del self.names[indx]
    
            del self.final_accuracies[indx]

    def sort_by_date(self):

        zipped = zip(self.datetimes, self.names, self.paths,self.epochs, self.lr, self.num_train_examples, self.img_paths, self.final_accuracies )

        zipped = sorted(zipped, key=lambda x: x[0])

        zipped = reversed(zipped)

        unzipped = list(zip(*zipped))

        unzipped = [list(var) for var in unzipped]

        self.datetimes, self.names, self.paths,self.epochs, self.lr, self.num_train_examples, self.img_paths, self.final_accuracies = unzipped



class Datasets_Info():

    def __init__(self):
        self.names = []
        self.num_samples = []
        self.paths = []
        self.datetimes = []


    def add_data(self, path, name):

        path = os.path.join(path,name)
        self.paths.append(path)

        name = os.path.splitext(name)[0]

        values = name.split("_")

        if len(values)<3:

            self.datetimes.append(values[1])

            self.names.append("No Name")

            self.num_samples.append(values[0])
        
        else : 
            self.datetimes.append(values[2])

            self.names.append(values[1])

            self.num_samples.append(values[0])

    def sort_by_date(self):

        zipped = zip(self.datetimes, self.names, self.num_samples, self.paths)

        zipped = sorted(zipped, key=lambda x: x[0])

        zipped = reversed(zipped)

        unzipped = list(zip(*zipped))

        unzipped = [list(var) for var in unzipped]

        self.datetimes, self.names, self.num_samples, self.paths = unzipped
