
from datetime import datetime
import numpy as np


class Kitchen:

    def __init__(self, food_labels, name):
        """A class to run the functions of a kitchen

        Args:
            food_labels (list): list of food labels
            name (string): name of kitchen
        """

        self.food_data = [0]*len(food_labels)
        self.name = name
        self.last_modified = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        self.food_labels = food_labels
        


    def set_foods(self, new_food_data):
        """Sets the food data and updates the last_modified to now

        Args:
            new_food_data (list): list of food values
        """

        self.food_data = new_food_data
        self.last_modified = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")


        
    def restore_kitchen(self, food_data, last_modified):
        """Restores a kitchen to data and time provided

        Args:
            food_data (list): list of food values
            last_modified (timestamps): a timestamp of last modified
        """
        self.food_data = food_data
        self.last_modified = last_modified


    def get_display_val(self):
        """Returns a dict of the values and labels to  be displayed by flask

        Returns:
            dict: a dict of all the food labels and their values (e.g. display_val['fruit'] == 100g)
        """
   
        display_val = {}

        for key, val in zip(self.food_labels,self.food_data):
            display_val[key]=val

        display_val['last_modified']=self.last_modified

        return display_val



def get_kitchens( kitchens):
    """Gathers all the kitchens values and returns them as an np array

    Returns:
        np array: a numpy array of all the kitchens stocks shape (num kitchen, num items)
    """

    np_kitchen_data = []

    for kitchen in kitchens:
        np_kitchen_data.append(kitchen.food_data)

    return np.array(np_kitchen_data, dtype=np.float32)

def check_kitchen_indx(this_kitchen, kitchens):
    """returns the index of the this_kitchen in the json data

    Args:
        this_kitchen (sting): Name of a kitchen

    Returns:
        int: The index of that kitchen in the json data
    """

    for indx, kitchen in enumerate(kitchens):

        if kitchen == this_kitchen:
            return indx
    
    return -1