

from datetime import datetime
import json
from os import path, mkdir, walk
from zipfile import ZipFile

import numpy as np


def setup_dirs():
        
        if path.isdir("./Data/Collected_Data") == False:
            mkdir("./Data/Collected_Data")

        if path.isdir("./Data/participant_labeling") == False:
            mkdir("./Data/participant_labeling")

        if path.isdir("./tmp") == False:
            mkdir("./tmp")
        
        if path.isdir("./static/images/graphs/") == False:
            mkdir("./static/images/graphs/")

        

def load_JSON( file_name):
    """Loads a json at `file_name` into self.json 

    Args:
        file_name (string): path to json file
    """
    
    #Load data from a JSON file 
    with open(file_name,"r") as read_file:
        json_data = json.load(read_file)
        return json_data



def save_JSON(working_file,  json_data, time_stamp = False):
    """Saves the file at in the same place, or with a time stamp.

    Args:
        time_stamp (bool, optional): if true saves json file with a timestamp
    """

    file_name = path.splitext(working_file)[0]
    if time_stamp:
        file_name += "_"+datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
    file_name += ".json"

    #save data from a JSON file 
    with open(file_name,"w") as write_file:
        json.dump(json_data, write_file, indent=4, cls=NumpyArrayEncoder)


def zip_data():

    directory = './Data'

    # calling function to get all file paths in the directory 
    file_paths = get_all_file_paths(directory) 

    # printing the list of all files to be zipped 
    print('Following files will be zipped:') 
    for file_name in file_paths: 
        print(file_name) 

    # writing files to a zipfile 
    with ZipFile('./tmp/Data.zip','w') as zip: 
        # writing each file one by one 
        for file in file_paths: 
            zip.write(file) 

    print('All files zipped successfully!')


def get_all_file_paths(self, directory): 

    # initializing empty file paths list 
    file_paths = [] 

    # crawling through directory and subdirectories 
    for root, directories, files in walk(directory): 
        for filename in files: 
            # join the two strings in order to form the full filepath. 
            filepath = path.join(root, filename) 
            file_paths.append(filepath) 

    # returning all file paths 
    return file_paths     



class NumpyArrayEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NumpyArrayEncoder, self).default(obj)
        