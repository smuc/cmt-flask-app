
from datetime import datetime
import json
import numpy as np

from cmt.utils import NumpyArrayEncoder



def sort_data_4_labeling( CMT):
    """Sets up all the kitchen classes that have been read from the json data and restores their values if possible.
    """
    # create list of lists to store each kitchens data in
    sorted_data = [[] for _ in range(len(CMT.kitchens)+1)]
    # do the same but for holding the lens of the arrays
    data_lens = [0 for _ in range(len(CMT.kitchens)+1)]
    # get the json array data
    kitchen_data_array = CMT.json.get('kitchen_data')

    #if it exists
    if kitchen_data_array:
        # iterate over the data
        for kitchen_data in reversed(kitchen_data_array):

            food_data = np.array(kitchen_data['food_data'], dtype=np.float32)

            if np.sum(food_data) ==0:
                continue
            # get the label (index) of the data
            label = kitchen_data['kitchen_label'] 
            # append the food data to that kitchens list
            sorted_data[label].append(food_data)
            # add one to the size of that array
            data_lens[label] +=1

    else: # if it doesn't exist return
        return

    #get pickup data
    pickup_data_array = CMT.json.get('pickup_data')

    # if it exists
    if pickup_data_array:
        # loop over the entries
        for pickup_data in pickup_data_array:
            #then loop over the items
            for i, item_amount in enumerate(pickup_data['food_data']):
                #if they equal 0
                if item_amount ==0:
                    continue # skip

                # else create an array of  zeros length of num food items
                pickup_array = np.zeros(len(pickup_data['food_data']), dtype=np.float32)
                # then set this items index to the item amount for one hot encoding
                pickup_array[i] = item_amount
                # then append that array to the pickup list at the last index
                sorted_data[-1].append(pickup_array)
                # add one to the size of this array
                data_lens[-1] +=1

    else: # if it doesn't exist return
        return
    
    # setup list to hold meshing data
    to_mesh = []
    # iterate over the kitchen data lengths
    for  data in data_lens:
        # for each one append a range representing the indexes of that array
        to_mesh.append(np.arange(data, dtype=np.int8))

    # use meshgrid to create combinations of all of the indexes provided
    data_samples = np.array(np.meshgrid(*to_mesh)).T.reshape(-1, 7)
    

    data_samples = np.take(data_samples,np.random.permutation(data_samples.shape[0]),axis=0,out=data_samples)

    num_samples = 3000

    data_samples = data_samples[:num_samples]


    # create another list to hold the new samples
    new_data_samples = []
    
    # run through the mesh index combinations
    for sample in data_samples:

        # a list to hold this sample
        this_sample = []
        # enumerate over the sample indexes
        for kitchen_index, sample_index in enumerate(sample):
            # append the right data for this kitchen
            this_sample.append(sorted_data[kitchen_index][sample_index])

        # convert to an array
        this_sample = np.array(this_sample, dtype=np.float32)
        # append this as a new data sample
        new_data_samples.append(this_sample)
    
    new_data_samples = np.array(new_data_samples, dtype=np.float32)

    noise = np.random.normal(0,0.12, new_data_samples[:, :, 1:].shape)

    new_data_samples[:, :, 1:] = (new_data_samples[:, :, 1:] * (1+noise))

    new_data_samples = np.ceil(new_data_samples).astype(int)

    json_data = {}
    json_data['num_samples'] = num_samples
    json_data['food_data_train'] = new_data_samples
    json_data['food_labels'] = CMT.json['food_labels']
    json_data['kitchen_labels'] = CMT.json['kitchen_labels']
    
    path = "./static/data/labeling_data.json"
    with open(path,"w") as write_file:
            json.dump(json_data, write_file, cls=NumpyArrayEncoder, indent=4)
    
def save_labeled_data( json_data):

    name = json_data['name']

    ready_for_training = {}
    ready_for_training['food_labels'] = json_data['food_labels']
    ready_for_training['kitchen_labels'] = json_data['kitchen_labels']

    num_samples = len(json_data['food_data'])

    split = int(num_samples*0.8)

    ready_for_training['num_train'] = split
    ready_for_training['num_test'] = num_samples-split

    ready_for_training['food_data_train'] = json_data['food_data'][:split]
    ready_for_training['food_data_test'] = json_data['food_data'][split:]

    ready_for_training['kitchens_data_train'] = json_data['kitchens_data'][:split]
    ready_for_training['kitchens_data_test'] = json_data['kitchens_data'][split:]

    path = "./Data/participant_labeling/"+str(num_samples)+ "_" + name + "_" + datetime.now().strftime("%d-%m-%Y, %H:%M:%S")+".json"
    with open(path,"w") as write_file:
            json.dump(ready_for_training, write_file, indent=4)