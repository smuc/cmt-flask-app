
from datetime import datetime
from os import listdir, path, remove
from cmt.training import Models_Info
from cmt.utils import setup_dirs, save_JSON, load_JSON
from cmt.kitchens import get_kitchens, check_kitchen_indx, Kitchen
import numpy as np 
from smuc_kitchen.classify import classify
from smuc_kitchen.utils.save_load import load_model_for_inference

# CMT_Data class that handles all the processes for the apps form inputs, outputs and json saving
class CMT_Interface:

    def __init__(self, start_fresh=False):
        """CMT_Data class  handles all the processes for the apps form inputs from forms, managing the classifier and saving data to json.

        Args:
            file_name (sting): Path to save(d) data file
            start_fresh (bool, optional): If set to true it will reset data, and save the old (if there was one) with a timestamp.
        """
        setup_dirs()
        self.working_file = "./Data/Collected_Data/Real_Data.json"
        try:
            self.json = load_JSON(self.working_file)
            
            if start_fresh==True:
                save_JSON(self.working_file, self.json, time_stamp=True)
                self.json = load_JSON('./Data/Lables.json')

        except:
            self.json = load_JSON('./Data/Lables.json')

        self.setup_kitchens()       

        self.get_saved_models("./trained_models")

        self.load_model_to_run(self.model_infos.paths[0].split('/')[-1])
        
        self.training_now = False


    def add_kitchen_data(self, form_data):
        """Adds kitchen data to the class by saving it to the json and updating its kitchen class data

        Args:
            form_data (_type_): raw data from the flask form

        Returns:
            dict: A dictionary pairing of all the entered values and the labels (e.g. dict["fruit"] = 200g) to be displayed in flask
        """

        display_values = {}
        values = [] 
        
        for key, value in form_data:

            #Values is now dictionary data structure store values as a key-value pair.
            display_values[key] =value    

            if key != 'Kitchen':
                value = float(value)
            
            values.append(value)    

        kitchen_data = {}
        kitchen_index = check_kitchen_indx(values[0], self.json['kitchen_labels'])
        kitchen_data['kitchen_label'] = kitchen_index
        kitchen_data['food_data'] = values[1:]
        kitchen_data['time_stamp'] = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        self.kitchens[kitchen_index].set_foods(kitchen_data['food_data'])
        
        np_food_data = get_kitchens(self.kitchens)


        if "kitchen_data" in self.json:
            self.json["kitchen_data"].append(kitchen_data)
        else:
            self.json["kitchen_data"]=[kitchen_data]
   

        if "food_data" in self.json:
            self.json["food_data"].append(np_food_data)

        else:
            self.json["food_data"]=[np_food_data]

        save_JSON(self.working_file, self.json)

        return display_values
    

    def add_pickup_data(self, form_data, classify = True):
        """Adds pickup data to the json and also runs it through the classifier, and then returns the results so they can be displayed by flask. 

        Args:
            form_data (dict): Raw form data from flask keys and values for inputs

        Returns:
            dist: A dictionary pairing of all the classified values and the labels (e.g. dict["fruit"] = kitchen 3) to be displayed in flask
        """
        display_values = {}
        values = [0,0] 
        for key, value in form_data:

            value = float(value)
            
            values.append(value)    
            if value == 0:
                continue
            elif classify == False:
                display_values[key] =value  

        if classify == True:
            display_values, classified_data = self.run_classify(values)


        pickup_data = {}
        pickup_data['food_data'] = values
        pickup_data['time_stamp'] = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")

        if "pickup_data" in self.json:
            self.json["pickup_data"].append(pickup_data)
        else:
            self.json["pickup_data"]=[pickup_data]

        if classify and "classified_data" in self.json:
            self.json["classified_data"].append(classified_data)

        elif classify:
            self.json["classified_data"]=[classified_data]

        save_JSON(self.working_file, self.json)

        return display_values

    
    def setup_kitchens(self):
        """Sets up all the kitchen classes that have been read from the json data and restores their values if possible.
        """

        self.kitchens = [Kitchen(self.json['food_labels'], name) for name in self.json['kitchen_labels']]

        indxs = np.zeros(len(self.kitchens), dtype=np.int32)

        kitchen_data_array = self.json.get('kitchen_data')

        if kitchen_data_array:

            for kitchen_data in reversed(kitchen_data_array):

                label = kitchen_data['kitchen_label'] 

                if indxs[label] == 0:
                    indxs[label] = 1
                    self.kitchens[label].restore_kitchen(kitchen_data['food_data'], kitchen_data['time_stamp'])
                    
                if np.sum(indxs) == len(self.kitchens):
                    return

    def stack_data(self, values, kitchens):
        """ Stacks the pickup values onto the kitchen data ready for classification.
        Args:
            values (list): a list of the picked up values

        Returns:
            _type_: an np array of the kitchens and the picked up items in a one hot encoding shape (num items, kitchens, num items), ready for classification.
        """
         
        np_classify_data = []
        
        np_kitchen_data = get_kitchens(kitchens)
        
        for i, value in enumerate(values):
            this_value = np.zeros(shape=[len(values)])
            this_value[i] = value
            this_value = np.vstack((np_kitchen_data, this_value))
            np_classify_data.append(this_value)
        
        np_classify_data = np.array(np_classify_data)
   
        return np_classify_data


    def run_classify(self, values):
        """Takes in the values and then forms them into the right array shape for the model, runs it through classifier to be displayed and saved

        Args:
            values (list): A list of the food values taken from the form

        Returns:
            tuple {dict|dict}: two dictionaries the first holding the key value pairs for flask to display. The second all of the input/outputs of the  classification.
        """

        np_classify_data = self.stack_data(values, self.kitchens)

        raw_classifications = classify(self.model , np_classify_data)

        classifications = raw_classifications.argmax(axis=1)

        classification_data =  {}

        classification_data['input'] = np_classify_data
        classification_data['raw_classification'] = raw_classifications
        classification_data['classification'] = classifications
        classification_data['time_stamp'] = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")

        display_values = {}

        for i, classification in enumerate(classifications):

            if values[i] == 0:
                continue

            kitchen_label = self.json['kitchen_labels'][classification]
            food_label = self.json['food_labels'][i]
            display_values[food_label] = kitchen_label

        return display_values, classification_data
    
    def get_saved_models(self, this_path):
        
        file_names = listdir(this_path)

        self.model_infos = Models_Info()


        for name in file_names:
            self.model_infos.add_model_from_file( this_path, name)

        self.model_infos.sort_by_date()


        return self.model_infos
    
    def delete_model(self, name):

        img_path = path.join('./static/images/graphs/',name+'.png')

        model_path = path.join('./trained_models/', name)
        
        if path.exists(img_path):
            remove(img_path)
        if path.exists(model_path):
            remove(model_path)

        self.model_infos.delete_model(name)

    def load_model_to_run(self, name):

        model_path = path.join('./trained_models/', name)

        self.running_model_name = name
        
        if path.exists(model_path):
            self.model, self.model_summary = load_model_for_inference(model_path)
