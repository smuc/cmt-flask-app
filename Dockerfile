# This file is a template, and might need editing before it works on your project.
FROM python:3.10.6-bullseye

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
# RUN apk --no-cache add postgresql-client

WORKDIR /usr/src/app
COPY . /usr/src/app

RUN pip install --no-cache-dir -r requirements.txt


EXPOSE 5000
ENTRYPOINT ["flask", "--app", "app", "run", "--host=0.0.0.0"]