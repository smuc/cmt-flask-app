# CMT-Flask-App

A flask app for the CMT test run

To test run:
- Install `requirement.txt`  with pip
- Run `flask --app app run --host=0.0.0.0` to run it.
- open the port it prints out.
- use the password it prints out to login. 


TODO:

- [x] in training class edit the function we made to pull files and display them in the file selection. to do this do:
    - [x] use glob or os to get files from that dir
    - [x] break the file names into the ellements (num samples, name, datetime)
    - [x] return these ellements to be rendered in flask

- [X] Add hardcoded hyperparemters:
    - [X] In training class add new function called 'get_hyper_params()'
    - [X] In it declare a list and add the values:
        - LR (learning rate) e.g. 0.0008 (range 0.001 to 0.00001)
        - epochs e.g. int 300 - 1000
        - layers eg list of ints [128, 128]
        - model_type (will be drop down)
        - name -> str (for saving, not included in the train_model function handle)
    - [X] get it to return that list when called


- [X] In flaskform use the two functions above and the list of models (for the drop down) to get the hyper params and the list of models and list of data files and render it in the template with:
    - [X] the file info next to a tick boxes.
    - [X] the hyper params with number inputs next to them (model type with a drop down)
    - [X] a train button


- [X] when submitted the training info (pressing train):
    - [X] formflask hands the tickbox info and hyper perameters to the training class through a function function called 'train' (e.g. training.train(tickbox_info, hyperperemeters)).
    G - [X] the train function then, Uses the tick box info to create a list of the selected files.
   M - [X] the html etc will show a loading wheel until it is redirected to the model selection page (maybe use a dummy pause function for the time being).
   - [x] tweak the <p> text I added to the loading message and make it to be slightly smaller and bellow. 

- [X] A new page for model selection. just prepping atm and similar to training interface but with each dropdown being a trained model with
    - [X] the title being the model name
    - [X] has an image of the graph from static file (can use dummy atm)
    - [X] text underneath from summary the dict showing training values.
    - [X] a select button to choose that model (just has a js trigger to print model name or similar atm).
    - [x] style the two buttons for "use" and "delete" to fit in - M
    - [x] maybe highlight the selected models tab/dropdown  - M




